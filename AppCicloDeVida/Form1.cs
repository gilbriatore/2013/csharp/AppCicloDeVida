﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppCicloDeVida
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void Log(string s)
        {
            Console.WriteLine(s);
        }

        int contador = 0;

        private void Form1_Load(object sender, EventArgs e)
        {
            contador++;
            Log(contador + " - Form1_Load");
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            contador++;
            Log(contador + " - Form1_Paint");
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            contador++;
            Log(contador + " - Form1_FormClosed");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            contador++;
            Log(contador + " - Form1_FormClosing");
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            contador++;
            Log(contador + " - Form1_Shown");
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            contador++;
            Log(contador + " - Form1_Activated");
        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
            contador++;
            Log(contador + " - Form1_Deactivate");
        }

        private void Form1_Move(object sender, EventArgs e)
        {
            Log("Margem esquerda: " + this.Left);
            Log("Margem superior: " + this.Top);

        }
    }
}
